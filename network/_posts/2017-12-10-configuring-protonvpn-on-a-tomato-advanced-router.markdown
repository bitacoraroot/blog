---
title: "Configuring ProtonVPN on a Tomato Advanced router"
excerpt: "Configure your router to redirect traffic to your VPN."
date: "2017-12-10 14:33:53 +0100"
tags: [ProtonVPN, VPN, Tomato, Router]
---

## Subscribe to ProtonVPN

Once you have subscribed to ProtonVPN download your configuration by logging in
[ProtonVPN](https://protonvpn.com) and going in `Downloads`.
There select your platform: DD-WRT router (which also works for Tomato).
Select the `UPD` protocol.
And choose your configuration (Secure Core + Country).

You can also download all configurations if you want to.

You will get a configuration file that looks like this:

{% highlight2 ini caption=se-hk-01.protonvpn.com.udp1194.ovpn %}
client
dev tun
proto udp

remote 185.188.122.11 1194
remote 185.188.122.12 1194
server-poll-timeout 20

remote-random
resolv-retry infinite
nobind
cipher AES-256-CBC
auth SHA512
comp-lzo
verb 3

tun-mtu 1500
tun-mtu-extra 32
mssfix 1450
persist-key
persist-tun

ping 15
ping-restart 0
ping-timer-rem
reneg-sec 0

remote-cert-tls server
auth-user-pass
pull
fast-io


<ca>
-----BEGIN CERTIFICATE-----
long string describing your certificate
-----END CERTIFICATE-----
</ca>

key-direction 1
<tls-auth>
# 2048 bit OpenVPN static key
-----BEGIN OpenVPN Static key V1-----
long string describing your static key
-----END OpenVPN Static key V1-----
</tls-auth>
{% endhighlight2 %}

## Configure Tomato Advanced

Login to your router web interface.

### Basic
In `VPN > OpenVPN Client` select the Client 1 and the tab `Basic`.

| Option                              | Value                                                                              |
|-------------------------------------|------------------------------------------------------------------------------------|
| Start with WAN                      | Yes                                                                                |
| Interface Type                      | TUN                                                                                |
| Server address/Port                 | `se-hk-01.protonvpn.com` `1194` (info in the ovpn file name)                       |
| Firewall                            | Automatic                                                                          |
| Authorization mode                  | TLS                                                                                |
| Username                            | your username found in the `Account` section of [ProtonVPN](https://protonvpn.com) |
| Password                            | your password found in the `Account` section of [ProtonVPN](https://protonvpn.com) |
| Username Authen. Only               | No                                                                                 |
| Extra HMAC authorization (tls-auth) | Outgoing(1)                                                                        |
| Create NAT on tunnel                | Yes                                                                                |

### Advanced
In `VPN > OpenVPN Client` select the Client 1 and the tab `Advanced`.

| Option                                 | Value       |
|----------------------------------------|-------------|
| Poll interval                          | 0           |
| Ignore Redirect Gateway (route-nopull) | Yes         |
| Accept DNS configuration               | Strict      |
| Encryption cipher                      | AES-256-CBC |
| Compression                            | Enabled     |
| TLS Renegotiation Time                 | -1          |
| Connection retry                       | -1          |
| Verify server certificate (tls-remote) | No          |
| Custom configuration                   | see below   |
{% highlight2 ini caption=custom_config %}
remote 185.188.122.11 1194
remote 185.188.122.12 1194
server-poll-timeout 20
remote-random
resolv-retry infinite
nobind
auth SHA512
tun-mtu 1500
tun-mtu-extra 32
mssfix 1450
ping 15
ping-restart 0
ping-timer-rem
reneg-sec 0
remote-cert-tls server
pull
fast-io
{% endhighlight2 %}


### Keys
In `VPN > OpenVPN Client` select the Client 1 and the tab `Keys`.

| Text box | Content |
| ----     | ----    |
| Static Key | Copy the static key from the configuration file downloaded earlier. It starts with `-----BEGIN OpenVPN Static key V1-----` and ends with `-----END OpenVPN Static key V1-----` |
| Certificate Authority | Copy the certificate authority from the configuration file downloaded earlier. It starts with `-----BEGIN CERTIFICATE-----` and ends with `-----END CERTIFICATE-----` |

### Routing Policy
In `VPN > OpenVPN Client` select the Client 1 and the tab `Routing Policy`.

* Check `Redirect through VPN`.

Add a rule for each subnetwork or ip address you want to see redirected.

#### Example


| Action                                                                  | Enabled | Type              | Value          |
| ------------------------------------------------------------------------|---------|-------------------|----------------|
| Redirect all machines from the network 192.168.1.0/16 to the VPN tunnel | Yes     | From source IP    | 192.168.1.0/16 |
| Redirect all machines from the network 192.168.2.0/16 to the VPN tunnel | Yes     | From source IP    | 192.168.2.0/16 |
| Redirect all connection going to 92.34.55.12 to the VPN tunnel          | Yes     | To destination IP | 92.34.55.12    |

### Save and start

Click on `Save`, then start the VPN client by clicking on the run button.
You can then go to the tab `Status` that should look like this:

#### General Statistics

| Name                  |	Value     |
|-----------------------|-----------|
| TUN/TAP read bytes    |	111503667 |
| TUN/TAP write bytes   |	43557638  |
| TCP/UDP read bytes	  | 54588985  |
| TCP/UDP write bytes	  | 121331606 |
| Auth read bytes	      | 43557690  |
| pre-compress bytes	  | 109959751 |
| post-compress bytes	  | 109252713 |
| pre-decompress bytes  | 67501     |
| post-decompress bytes | 73461     |
