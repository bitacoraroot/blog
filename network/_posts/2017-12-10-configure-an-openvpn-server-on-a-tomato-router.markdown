---
title: "Configure an OpenVPN server on a Tomato router"
excerpt: "Generate certificates, and configure your Tomato router to host an OpenVPN server."
date: "2017-12-10 16:18:56 +0100"
tags: [OpenVPN, VPN, Tomato, Router]
---

## Creating the Keys

Steps required :
 1. Installing and setting up [EasyRSA](https://github.com/OpenVPN/easy-rsa)
 2. Generating a Certificate Authority (CA) cert (`ca.crt` and `ca.key`)
 3. Generating a server key (`server.key`) and a server certificate (`server.crt`)
 4. Generating the Diffie-Hellman parameters (`dh.pem`)

### Files description

| Filename   | Required by| Private | Description                            |
|------------|------------|---------|----------------------------------------|
| ca.crt     | All        | No      | Root CA certificate                    |
| ca.key     | Key signer | Yes     | Root CA key (used to signed)           |
| dh.pem     | Server     | No      | Diffie Hellman parameters              |
| server.crt | Server     | No      | Server certificate                     |
| server.key | Client     | Yes     | Server key                             |
| client.crt | Client     | No      | Client certificate                     |
| client.key | Client     | Yes     | Client key                             |

## Installing EasyRSA

Download EasyRSA from its Github repository:
~~~bash
cd ~
git clone git@github.com:OpenVPN/easy-rsa.git
cd easy-rsa/easyrsa3
~~~

Create a `vars` file from the example given:
~~~bash
cp vars.example vars
~~~

Edit it the options according to your location
{% highlight2 ini caption=vars %}
set_var EASYRSA_REQ_COUNTRY "PL"
set_var EASYRSA_REQ_PROVINCE "Malopolska"
set_var EASYRSA_REQ_CITY    "Cracow"
set_var EASYRSA_REQ_ORG     "SuperName"
set_var EASYRSA_REQ_EMAIL   "email"
set_var EASYRSA_REQ_OU      "Somename"
{% endhighlight2 %}


## Generating the CA certificate

Initialize the Public Key Infrastructure with `init-pki`
~~~bash
./easyrsa init-pki

# Output
Note: using Easy-RSA configuration from: ./vars

init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /home/user/openvpn/easy-rsa/easyrsa3/pki
~~~

Generate the certificate with `build-ca`:
~~~bash
./easyrsa build-ca

# Output
Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
.............................................................+++
...........................+++
writing new private key to '/home/user/openvpn/easy-rsa/easyrsa3/pki/private/ca.key'
Enter PEM pass phrase: PROVIDE PASSWORD HERE AND PRESS ENTER
Verifying - Enter PEM pass phrase: PROVIDE PASSWORD AGAIN

You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]: PROVIDE A NAME YOU WANT

CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/home/user/openvpn/easy-rsa/easyrsa3/pki/ca.crt
~~~

Here are the files at our disposal now :

| Filename   | Required by| Private | Path relative to easyrsa3              |
|------------|------------|---------|----------------------------------------|
| ca.crt     | All        | No      | pki/ca.crt                             |
| ca.key     | Key signer | Yes     | pki/private/ca.key                     |


## Generating the server key and certificate

{% highlight2 bash caption=easyrsa %}
./easyrsa build-server-full SERVERNAME

Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
......+++
..................+++
writing new private key to '/home/user/openvpn/easy-rsa/easyrsa3/pki/private/SERVERNAME.key'
Enter PEM pass phrase: PROVIDE A PASSWORD
Verifying - Enter PEM pass phrase:
-----
Using configuration from /home/user/openvpn/easy-rsa/easyrsa3/openssl-1.0.cnf
Enter pass phrase for /home/user/openvpn/easy-rsa/easyrsa3/pki/private/ca.key: CA PASSWORD

Check that the request matches the signature
Signature ok
The Subjects Distinguished Name is as follows
commonName            :PRINTABLE:'SERVERNAME'
Certificate is to be certified until Jul 17 14:55:45 2024 GMT (3650 days)

Write out database with 1 new entries
Data Base updated
{% endhighlight2 %}

Here are the files we just created:

| Filename       | Required by| Private | Path relative to easyrsa3           |
|----------------|------------|---------|-------------------------------------|
| SERVERNAME.crt | Server     | No      | pki/issued/SERVERNAME.crt           |
| SERVERNAME.key | Key signer | Yes     | pki/private/SERVERNAME.key          |


## Passwordless server key

Here is our problem:
 1. Tomato VPN cannot handle password protected server keys.
 2. It is impossible to generate a passwordless server key and certificate without causing an error.

This is why we generated a server key that is password protected. And now we will derive a new key that is not password protected.

~~~bash
cd pki/private
openssl rsa -in SERVERNAM.key -out SERVERNAME_nopass.key
# Output
Enter pass phrase for SERVERNAME.key: SERVER KEY PASSWORD
writing RSA key
~~~

Here is the file we just created:

| Filename              | Required by|Private| Path relative to easyrsa3       |
|-----------------------|------------|-------|---------------------------------|
| SERVERNAME_nopass.key | Key signer | Yes   |pki/private/SERVERNAME_nopass.key|


## Generating Diffie-Hellman parameters

~~~bash
cd ~/easyrsa/easyrsa3
./easyrsa gen-dh
# Output
Note: using Easy-RSA configuration from: ./vars
Generating DH parameters, 2048 bit long safe prime, generator 2
This is going to take a long time
........................+..................
DH parameters of size 2048 created at /home/user/openvpn/easy-rsa/easyrsa3/pki/dh.pem
~~~


Here is the file we just created:

| Filename  | Required by|Private| Path relative to easyrsa3  |
|-----------|------------|-------|----------------------------|
| dh.pem    | Server     | No    | pki/dh.pem                 |


## VPN Server configuration

### Basic

`VPN > OpenVPN Server > VPN Server 1 > Basic`

| Option                               | Value                           |
|--------------------------------------|---------------------------------|
| Start with WAN                       | Yes                             |
| Interface type                       | TAP                             |
| Bridge TAP with                      | LAN (br0)*                      |
| Protocol                             | UDP                             |
| Port                                 | 1194                            |
| Firewall                             | Automatic                       |
| Authorization mode                   | TLS                             |
| Extra HMAC authorization (tls-auth)  | Disabled                        |
| Client address pool                  | No DHCP, 10.0.8.2-10.0.8.254 |


### Advanced

`VPN > OpenVPN Server > VPN Server 1 > Advanced`

| Option                                      | Value                           |
|---------------------------------------------|---------------------------------|
| Poll Interval                               | 0                               |
| Direct clients to redirect Internet traffic | Yes                             |
| Respond to DNS                              | Yes                             |
| Advertise DNS to clients                    | Yes                             |
| Encryption cipher                           | Use Default                     |
| Compression                                 | Disabled                        |
| TLS Renegotiation Time                      | -1                              |
| Manage Client-Specific Options              | No                              |
| Allow User/Pass Auth                        | No                              |
| Custom configuration                        |                                 |



### Keys

`VPN > OpenVPN Server > VPN Server 1 > Keys`

| Text box                  | Value                           |
|---------------------------|---------------------------------|
| Certificate Authority     | Contents of `ca.crt`            |
| Server Certificate        | Contents of `server.crt`        |
| Server Keys               | Contents of `server_nopass.key` |
| Diffie Hellman parameters | Contents of `dh.pem`            |


### Save and start

 1. Click `Save`.
 2. Start the server using the run button : <i class="fa fa-fw fa-play" />
 3. Go to  `VPN > OpenVPN Server > VPN Server 1 > Status` and verify that
 the output is approximately similar to the one below.

##### General statistics

| Name                        | Value     |
|-----------------------------|-----------|
| Mx bcast/mcast queue length | 2         |


### Storing the private keys

To store your private keys, you can use a USB key encrypted with `LUKS`.
You can also use the `Attach File` option in `KeePass` to attach your `.crt` and `.key` files.

## Generating a client certificate

~~~bash
./easyrsa build-client-full CLIENTNAME
~~~

{% highlight2 bash caption=Output %}
Note: using Easy-RSA configuration from: ./vars
Generating a 2048 bit RSA private key
...............+++
writing new private key to '/home/user/openvpn/easy-rsa/easyrsa3/pki/private/CLIENTNAME.key'
Enter PEM pass phrase: CLIENT PASSWORD
Verifying - Enter PEM pass phrase:
-----
Using configuration from /home/user/openvpn/easy-rsa/easyrsa3/openssl-1.0.cnf
Enter pass phrase for /home/user/openvpn/easy-rsa/easyrsa3/pki/private/ca.key: ENTER CA PASSWORD HERE
Check that the request matches the signature
Signature ok
The Subjects Distinguished Name is as follows
commonName            :PRINTABLE:'CLIENTNAME'
Certificate is to be certified until Jul 17 15:21:08 2024 GMT (3650 days)

Write out database with 1 new entries
Data Base Updated
{% endhighlight2 %}

Here are the files we just created:

| Filename       | Required by| Private | Path relative to easyrsa3           |
|----------------|------------|---------|-------------------------------------|
| CLIENTNAME.crt | Client     | No      | pki/issued/CLIENTNAME.crt           |
| CLIENTNAME.key | Client     | Yes     | pki/private/CLIENTNAME.key          |
