---
title: "Hauppauge WinTv-SoloHD on Raspbian"
excerpt: "Broadcasting TV to your home network."
date: "2017-10-22 23:01:54 +0200"
---

It turns out it is extremely difficult to use the Hauppauge WinTv-SoloHD key
with Raspbian.

## Problem description

1. The DVB-T tuner Hauppauge WinTv-SoloHD does not work on Raspbian (kernel 4.1.7+)
2. The DVB-T tuner is supposed to work on linux from kernel version 3.16[^1] [^2]

{% include figure image_path="/assets/images/network/hauppauge_website_linux.PNG" alt="Screen capture of hauppauge website" caption="Proof that Hauppauge supports Linux." %}


[^1]: http://www.hauppauge.co.uk/site/products/data_solohd.html
[^2]: http://www.linuxtv.org/wiki/index.php/PCTV_Systems_tripleStick_T2_%28292e%29

The Hauppauge website redirects to [linuxtv.org](http://www.linuxtv.org/wiki/index.php/PCTV_Systems_tripleStick_T2_%28292e%29) with their link "For more information, please see here".

On this linuxtv we can see that the device should be recognized:
 * it is supported by linux since 2015-11-19
 * the [link to the commit](http://git.linuxtv.org/%E2%80%A6) that made the DVB-T tuner work in the linux source code is available.

{% include figure image_path="/assets/images/network/linuxtv_pctv292e.PNG" alt="Screen capture of linuxtv website" caption="Information from linuxtv" %}

{% include figure image_path="/assets/images/network/hauppauge_website_linux.PNG" alt="Screen capture of linuxtv website" caption="Commit linked on linuxtv" %}

## Analysis

If we take a look to see which lines were modified in the [source code](http://git.linuxtv.org/media_tree.git/commit/?id=1efc21701d94ed0c5b91467b042bed8b8becd5cc) we see that two files were modified:
 * drivers/media/dvb-core/dvb-usb-ids.h
 * drivers/media/usb/em28xx-cards.c

~~~diff
author	Arno Bauernöppel <arno@aziraphale.net>	2015-11-15 21:24:10 (GMT)
committer	Mauro Carvalho Chehab <mchehab@osg.samsung.com>	2015-11-19 13:34:32 (GMT)
commit	1efc21701d94ed0c5b91467b042bed8b8becd5cc (patch)
tree	ba79b687ca3246b1abafa4c2bdbedde43052a591
parent	b833d0df943d70682e288c38c96b8e7bfff4023a (diff)
[media] Add support for dvb usb stick Hauppauge WinTV-soloHD
This patch adds support for the DVB-T/C/T2 usb stick WinTV-soloHD from
Hauppauge. It adds the usb ID 2040:0264 Hauppauge to the cards of the
driver em28xx.

I successfully tested DVB-T/C and the IR remote control with the
firmware dvb-demod-si2168-b40-01.fw.

Signed-off-by: Arno Bauernoeppel <arno@aziraphale.net>
Signed-off-by: Mauro Carvalho Chehab <mchehab@osg.samsung.com>
Diffstat
-rw-r--r--	drivers/media/dvb-core/dvb-usb-ids.h	1

-rw-r--r--	drivers/media/usb/em28xx/em28xx-cards.c	2

2 files changed, 3 insertions, 0 deletions
~~~

### dvb-usb-ids.h
A diff on the dvb­usb­ids.h file gives the following result:

~~~diff
diff --git a/drivers/media/dvb-core/dvb-usb-ids.h b/drivers/media/dvb-core/dvb-usb-ids.h
index 0a46580..1c1c298 100644
--- a/drivers/media/dvb-core/dvb-usb-ids.h
+++ b/drivers/media/dvb-core/dvb-usb-ids.h
@@ -389,4 +389,5 @@
 #define USB_PID_PCTV_2002E_SE                           0x025d
 #define USB_PID_SVEON_STV27                             0xd3af
 #define USB_PID_TURBOX_DTT_2000                         0xd3a4
+#define USB_PID_WINTV_SOLOHD                            0x0264
 #endif
~~~
We see that a line was added to identify the product with the id 0264 as the DVB­T Tuner WinTv­SoloHD.

### em28xx­cards.c
A diff on the em28xx­cards.c gives the following result:

~~~diff
diff --git a/drivers/media/usb/em28xx/em28xx-cards.c b/drivers/media/usb/em28xx/em28xx-cards.c
index 5373dce..a1b6ef5 100644
--- a/drivers/media/usb/em28xx/em28xx-cards.c
+++ b/drivers/media/usb/em28xx/em28xx-cards.c
@@ -2475,6 +2475,8 @@ struct usb_device_id em28xx_id_table[] = {
 			.driver_info = EM28178_BOARD_PCTV_461E },
 	{ USB_DEVICE(0x2013, 0x025f),
 			.driver_info = EM28178_BOARD_PCTV_292E },
+	{ USB_DEVICE(0x2040, 0x0264), /* Hauppauge WinTV-soloHD */
+			.driver_info = EM28178_BOARD_PCTV_292E },
 	{ USB_DEVICE(0x0413, 0x6f07),
 			.driver_info = EM2861_BOARD_LEADTEK_VC100 },
 	{ USB_DEVICE(0xeb1a, 0x8179),
~~~

We can see that a line was added to indicate that the usb device with the manufacturer id 2040
(Hauppauge) and the product id 0264 (WinTv­SoloHD) should use the driver for the
EM28178_BOARD_PCTV_292E.

This information is coherent with the links seen before.

So why the DVB­T tuner does not work on raspbian?

## Linux source code

Let’s check the linux source code, to verify that the files were really modified.  

We find the source code for the first file ​[here](https://github.com/torvalds/linux/blob/master/drivers/media/dvb-core/dvb-usb-ids.h):

{% highlight2 c caption=dvb-usb-ids.h %}
#define USB_PID_PCTV_2002E_SE                           0x025d
#define USB_PID_SVEON_STV27                             0xd3af
#define USB_PID_TURBOX_DTT_2000                         0xd3a4
#define USB_PID_WINTV_SOLOHD                            0x0264
#define USB_PID_EVOLVEO_XTRATV_STICK	0xa115
{% endhighlight2 %}

We can see that we do have the `#define USB_PID_WINTV_SOLOHD`.

Now if we check the source code for the [second file](https://github.com/torvalds/linux/blob/master/drivers/media/usb/em28xx/em28xx-cards.c) we see again that the source code is correct.

{% highlight2 c caption=em28xx-cards.capitalize %}
{ USB_DEVICE(0x2013, 0x0258),
    .driver_info = EM28178_BOARD_PCTV_461E },
{ USB_DEVICE(0x2013, 0x025f),
    .driver_info = EM28178_BOARD_PCTV_292E },
{ USB_DEVICE(0x2040, 0x0264), /* Hauppauge WinTV-soloHD */
    .driver_info = EM28178_BOARD_PCTV_292E },
{ USB_DEVICE(0x0413, 0x6f07),
{% endhighlight2 %}

Actually we even see that the last commit was made specifically to integrate our DVB-T Tuner:

{% include figure image_path="/assets/images/network/github_linux_em28xx_commit.PNG" alt="Screen capture of github website" caption="Commit for WinTV-SoloHD" %}

## Raspbian source code

We saw that the linux source code was correct. But what we really use on our Raspberry Pi is Raspbian. And if we check the [Raspbian source code](https://github.com/raspberrypi/linux/blob/rpi-4.1.y/drivers/media/dvb-core/dvb-usb-ids.h), we can see that there is no mention of "WinTV" anywhere. So here lies the root of our problem: the Raspbian source code does not use the latest linux version.

## Solution

The solution consists in modifying the Raspbian source code to add the commits adding support to the WinTv-SoloHD.
After downloading and modifying the source code, we will have to compile the new Raspbian image and create our Raspberry Pi image from this custom Raspbian image.
