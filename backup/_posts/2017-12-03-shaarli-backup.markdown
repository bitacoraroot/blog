---
title: "Shaarli backup"
date: "2017-12-03 13:58:46 +0100"
excerpt: "Backup your Shaarli data"
tags: [Shaarli, Backup]
---

## Official documentation

From the [official documentation](https://shaarli.readthedocs.io/en/master/Upgrade-and-migration/#backup-your-data).

Shaarli stores all user data under the data directory:

  * data/config.php - main configuration file
  * data/datastore.php - bookmarked links
  * data/ipbans.php - banned IP addresses
  * data/updates.txt - contains all automatic update to the configuration and datastore files already run

See [Shaarli configuration](https://shaarli.readthedocs.io/en/master/Shaarli-configuration) for more information about Shaarli resources.

It is recommended to backup this repository before starting updating/upgrading Shaarli:

  * users with SSH access: copy or archive the directory to a temporary location
  * users with FTP access: download a local copy of your Shaarli installation using your favourite client

## Creating a cron job

If you haven't already you might want to follow the post [Automate backups with
a Raspberry Pi]({% post_url backup/2017-11-12-automate-backups-with-a-raspberry-pi %}) where we create a
user `backupuser` in a chroot jail.

The cron job is simple, we just need to copy in `/home/backupuser` :
 - the `data` directory
 - the `version.php` file

First we login as root since the cron job will be run by the root user.
~~~bash
sudo su
~~~

Then we edit the crontab with `crontab -e`:
~~~ini
# Shaarli backup
10 23 * * * DATE=$(date +'\%Y-\%m-\%d-\%H\%M'); tar -zcf /home/backupuser/backup/shaarli/${DATE}_backup_shaarli.tar.gz /var/www/shaarli.myServer.com/data
~~~

We also add a command to remove old backups regularly:

~~~ini
# Shaarli cleanup
0 11 * * * BDIR=/home/backupuser/backup/shaarli; LAST=$(find ${BDIR} -type f -printf '\%f\n' | sort -nr | head -1); find ${BDIR} ! -name "${LAST}" -type f -exec rm -f {} +
~~~
