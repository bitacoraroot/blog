---
title: "Solving QNAP NAS network issues"
excerpt: "What to do when your NAS is unreachable"
tags: [QNAP, NAS, Network, Issues]
type: Document
---

What to do when you have network issues on your QNAP NAS ?

## Evaluating the damage

First off, it might be nice to connect the NAS to a screen and a USB keyboard.
With `ALT+F2` you can use a terminal and connect with an administrator account.
If there is no terminal, well this is really bad, and this is not the correct
documentation for this problem.

If there is a terminal, you can investigate.

### Ping
From the NAS :
* Can you ping your router ?
* Can you ping another machine on the same network ?
* Can you ping another machine on the same network but a different medium
(Wi-Fi instead of Ethernet) ?
* Can you do part of, or all of the above, but with host names instead of IP
addresses ?
* Can you ping a machine on the internet (try a popular DNS IP address) with its
IP ?
* Can you do the same with its host name ?

From another machine :
* Can your router ping the NAS ?
* Can a machine from your network ping the NAS ? On a different medium too ?

If the NAS can ping the router, then the network card is still working,
the problem might come from a firewall or a bad network configuration.

## Performing a basic reset

If it appears that the problem comes from a bad network configuration, performing
a basic reset will solve the problem.

### Procedure
To do so, press the reset button for approximately **3 seconds**.
You should hear *one* long beep.
Let the NAS do its thing, then try to connect to it.

{:.note .warning}
**Warning**: If you press the button
for 10 seconds, you will hear two beeps and the factory reset will start, which
will lead to the <u>complete loss</u> of your data

### Consequences
After pressing the reset button for 3 seconds, a beep sound will be heard. The following settings will be reset to default:
* 	System administration password: admin.
* 	TCP/IP configuration: Obtain IP address settings automatically via DHCP.
* 	TCP/IP configuration: Disable Jumbo Frame.
* 	TCP/IP configuration: If port trunking is enabled (dual LAN models only), the port trunking mode will be reset to “Active Backup (Failover)”.
* 	System port: 8080 (system service port).
* 	Security level: Low (Allow all connections).
* 	LCD panel password: (blank)[^1].
* 	VLAN will be disabled.
* 	Service binding: All NAS services run on all available network interfaces.

[^1]: This feature is only provided by the NAS models with LCD panels. Please visit http://www.qnap.com for details.

### Where to go from there
If this does not work, you can perform a hard reset that will **<u>ERASE</u>** all your
data. Or you can investigate further as the problem might still come from a bad
network configuration that you might solve. See below for leads to where the
problem might lie.

## Ping and ARP

If the NAS can ping the router but the router cannot ping the NAS, it might be
that your configuration forbids ARP or Ping responses.

### Check if ARP is ignored
You can easily check your configuration with this command line:

~~~bash
  find /proc/sys/net/ipv4/conf/br0/arp_* -exec tail -n +1 {} +
~~~

The output should be something like this

~~~bash
==> /proc/sys/net/ipv4/conf/br0/arp_accept <==
0

==> /proc/sys/net/ipv4/conf/br0/arp_announce <==
0

==> /proc/sys/net/ipv4/conf/br0/arp_filter <==
0

==> /proc/sys/net/ipv4/conf/br0/arp_ignore <==
0

==> /proc/sys/net/ipv4/conf/br0/arp_notify <==
0
~~~

If some of the files do not contain the default value (0), then you can try
setting it to 0 and see if it solves your problem.

{% capture arp_configuration %}
<a href="{{ site.baseurl }}{% post_url 2017-05-13-ip-sysctl-variables %}#arp-configuration">
here</a>
{% endcapture %}

{:.note .info}
You can find the IP-Sysctl
values for ARP {{arp_configuration}}</div>


### Check if ICMP is ignored

You can see if ICMP requests are ignored with this command line.
~~~bash
  find /proc/sys/net/ipv4/icmp_echo_* -exec tail -n +1 {} +
~~~

The output should be something like this:

~~~bash
==> /proc/sys/net/ipv4/icmp_echo_ignore_all <==
0

==> /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts <==
1
~~~

{% capture icmp_configuration %}
<a href="{{ site.baseurl }}{% post_url 2017-05-13-ip-sysctl-variables %}#icmp-configuration">here</a>
{% endcapture %}

{:.note .info}
You can find IP-Sysctl values for ICMP {{icmp_configuration}}

## Routes

Another problem might be that the routes are not properly configured.
Use the following command to show the configured routed:

~~~bash
  $ netstat -nr
~~~

{% highlight2 bash caption=Output %}
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         10.0.0.1        0.0.0.0         UG        0 0          0 eth1
10.0.0.0        0.0.0.0         255.255.240.0   U         0 0          0 eth1
127.0.0.0       0.0.0.0         255.0.0.0       U         0 0          0 lo
{% endhighlight2 %}

The default gateway if the line starting with `0.0.0.0`.
If you do not have a default gateway, then there is a problem. And you have to
manually add it.

Here is the command line to add a default gateway:

~~~bash
  ip route add default via 10.0.0.1
~~~

{:.note .info}
You can also use `route` and `ip route`
to show the routes
