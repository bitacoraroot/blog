---
title: "Restore your GPG keys backup"
excerpt: "You have lost your GPG keys and you need to restore your backups. I list the ways to do this."
date: "2017-10-22 10:25:18 +0200"
tags: [GPG, PGP, Key, Backup]
author_profile: false
sidebar:
  nav: "security-practices"
---

{% include toc %}

You have just finished creating your GPG keys backups, and to be sure that they
were not corrupted during the backup process, you want to test them. This
article explains how to restore your backed up GPG keys.

{% include warning.html title="Be careful of information leaks" content="As
with the procedure for key creation, you are advised to do these operations
on an air-gapped system." %}

## Restore USB encrypted backup

If you still have your encrypted USB backup, it will be the easiest way to
recover your files.

### Connect the USB drive
First, mount the USB on your **air-gapped** system and find its label:

~~~bash
$ dmesg | tail
[ 7667.607011] scsi8 : usb-storage 2-1:1.0
[ 7667.608766] usbcore: registered new interface driver usb-storage
[ 7668.874016] scsi 8:0:0:0: USB 0: 0 ANSI: 6
[ 7668.874242] sd 8:0:0:0: Attached scsi generic sg4 type 0
[ 7668.874682] sd 8:0:0:0: [sde] 62980096 512-byte logical blocks: (32.2 GB/30.0 GiB)
[ 7668.875022] sd 8:0:0:0: [sde] Write Protect is off
[ 7668.875023] sd 8:0:0:0: [sde] Mode Sense: 43 00 00 00
[ 7668.877939]  sde: sde1
[ 7668.879514] sd 8:0:0:0: [sde] Attached SCSI removable disk
~~~

Check the size to make sure it's the right drive:

~~~bash
$ sudo fdisk -l | grep /dev/sde
Disk /dev/sde: 30 GiB, 32245809152 bytes, 62980096 sectors
/dev/sde1        2048 62980095 62978048  30G  6 FAT16
~~~

### Mount the partition

Mount the partition:

~~~bash
$ sudo cryptsetup luksOpen /dev/sde1 encrypted-usb
Enter passphrase for /dev/sde1:
~~~

Mount the filesystem:

~~~bash
$ sudo mkdir /mnt/usb
$ sudo mount /dev/mapper/encrypted-usb /mnt/usb
~~~

### Transfer your files

Finally, copy files to it:

~~~bash
$ sudo cp -avi /mnt/usb/tmp.aaiTTovYgo $GNUPGHOME
‘/mnt/usb/tmp.aaiTTovYgo’ -> ‘/tmp/tmp.aaiTTovYgo’
‘/mnt/usb/tmp.aaiTTovYgo/revoke.txt’ -> ‘/tmp/tmp.aaiTTovYgo/revoke.txt’
‘/mnt/usb/tmp.aaiTTovYgo/gpg.conf’ -> ‘/tmp/tmp.aaiTTovYgo/gpg.conf’
‘/mnt/usb/tmp.aaiTTovYgo/trustdb.gpg’ -> ‘/tmp/tmp.aaiTTovYgo/trustdb.gpg’
‘/mnt/usb/tmp.aaiTTovYgo/random_seed’ -> ‘/tmp/tmp.aaiTTovYgo/random_seed’
‘/mnt/usb/tmp.aaiTTovYgo/master.key’ -> ‘/tmp/tmp.aaiTTovYgo/master.key’
‘/mnt/usb/tmp.aaiTTovYgo/secring.gpg’ -> ‘/tmp/tmp.aaiTTovYgo/secring.gpg’
‘/mnt/usb/tmp.aaiTTovYgo/mastersub.key’ -> ‘/tmp/tmp.aaiTTovYgo/mastersub.key’
‘/mnt/usb/tmp.aaiTTovYgo/sub.key’ -> ‘/tmp/tmp.aaiTTovYgo/sub.key’
‘/mnt/usb/tmp.aaiTTovYgo/pubring.gpg~’ -> ‘/tmp/tmp.aaiTTovYgo/pubring.gpg~’
‘/mnt/usb/tmp.aaiTTovYgo/pubring.gpg’ -> ‘/tmp/tmp.aaiTTovYgo/pubring.gpg’
~~~

Make sure the correct files were copied, then unmount and disconnect the encrypted USB drive:

~~~bash
$ sudo umount /mnt/usb
$ sudo cryptsetup luksClose encrypted-usb
~~~


## Restore paperkey backup

In this example, we try to restore the private master key from a paperkey backup.
This also works with private subkeys althought you might have to use a slightly
different command when importing the subkeys.

### Convert the public key

GPG 2.1 changes the keyring format to kbx. To make it work with paperkey, we have
to convert the public key back to `gpg`.

~~~bash
gpg2 --export > $GNUPGHOME/backup/pubring.gpg
~~~

### Recover the private master key

Then we use paperkey with the `--pubring` option to recover the private key.
~~~bash
cat private.master.key.paperkey | paperkey
--pubring $GNUPGHOME/backup/pubring.gpg > recovered.private.master.key.paperkey
~~~

### Import the recovered master key

Now that we have recovered our master key let's try to reimport it.
If you are testing your backups, you will have to move the existing private key
first:
~~~bash
mv $GNUPGHOME/private-keys-v1.d/C6B58F19A724CB3C4951E89D73DD9BFC4833F6C1.key
$GNUPGHOME/backup/
~~~

Now we that we are without a master key, we reimport it:

~~~bash
gpg2 --import $GNUPGHOME/backup/recovered.master.key
# Here we are prompted for the secret key password
gpg: WARNING: unsafe permissions on homedir '/tmp/keyCreation'
gpg: key 0x45B2745200A5D6B1: "John Doe <johndoe@mail.com>" not changed
gpg: key 0x45B2745200A5D6B1: secret key imported
gpg: Total number processed: 2
gpg:              unchanged: 1
gpg:       secret keys read: 2
gpg:   secret keys imported: 1
~~~~

{:.note .info}
You will be asked for your master key password to process the import

If we list the content of `$GNUPGHOME/private-keys-v1.d` we will see that our
master key is back.

## Restore a QR code backup

### Convert the QR codes to ASCII

First we convert the QR codes to ASCII with `zbarimg`.

~~~bash
cd $GNUPGHOME/backup
zbarimg -q -D --raw private.master.key.qrcode.1.png | head -n -1 > recovered.private.master.key.1.txt
zbarimg -q -D --raw private.master.key.qrcode.2.png | head -n -1 > recovered.private.master.key.2.txt
~~~

Here is a command to do it automatically:
~~~bash
i=0; for f in $(ls *.png); do echo "Decoding $f : file $i" \
 && zbarimg -q -D --raw $f | head -n -1 > recovered_$i.asc \
 && i=$(($i+1)); done
~~~

{:.note .info}
Note that `zbarimg` adds an empty line at the end of each file that we have to
remove.

Now we concatenate the outputs to have one master key file.

~~~bash
cp recovered.private.master.key.1.txt recovered.private.master.key.txt
cat recovered.private.master.key.2.txt >> recovered.private.master.key.txt
~~~

Here is a command to do it automatically, be sure to remove `recovered_total.asc`
if it exists first:
~~~bash
for f in $(ls *.asc | sort); do echo "Adding $f" && cat $f >> recovered_total.asc; done
~~~

### Import the recovered key

If you are in the process of testing your backups, you will have to the existing private key first:

~~~bash
mv $GNUPGHOME/private-keys-v1.d/C6B58F19A724CB3C4951E89D73DD9BFC4833F6C1.key
$GNUPGHOME/backup/
~~~

Then we import our private key :

~~~bash
gpg2 --import $GNUPGHOME/backup/recovered.private.master.key.txt
# Here we are prompted for the passphrase
gpg: WARNING: unsafe permissions on homedir '/tmp/keyCreation'
gpg: key 0x45B2745200A5D6B1: "John Doe <johndoe@mail.com>" not changed
gpg: key 0x45B2745200A5D6B1: secret key imported
gpg: Total number processed: 2
gpg:              unchanged: 1
gpg:       secret keys read: 2
gpg:   secret keys imported: 1
~~~
