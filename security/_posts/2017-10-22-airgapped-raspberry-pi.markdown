---
title: "Air-gapped Raspberry Pi"
excerpt: "An air gap is a network security measure to ensure that a computer network is physically isolated from unsecured networks."
tags: [Air-gap, Raspberry Pi]
date: "2017-10-22 09:50:11 +0200"
author_profile: false
sidebar:
  nav: "security-practices"
---


## Create the initial Raspbian image

### Download Raspbian

~~~bash
cd Downloads
unzip 2017-04-10-raspbian-jessie-lite.zip
# Output
Archive:  2017-04-10-raspbian-jessie-lite.zip
  inflating: 2017-04-10-raspbian-jessie-lite.img  
~~~

### Find the SD card

Plug the SD card and use `dmesg` to find it:
~~~bash
dmesg | tail
# Output
[  138.753330] CIFS VFS: Error connecting to socket. Aborting operation.
[  138.753386] CIFS VFS: Error connecting to socket. Aborting operation.
[  138.753690] CIFS VFS: cifs_mount failed w/return code = -113
[  138.753717] CIFS VFS: cifs_mount failed w/return code = -113
[  138.753725] CIFS VFS: cifs_mount failed w/return code = -113
[  138.753769] CIFS VFS: cifs_mount failed w/return code = -113
[ 4170.871943] sd 6:0:0:0: [sde] 125042688 512-byte logical blocks: (64.0 GB/59.6 GiB)
[ 4170.876516]  sde: sde1
~~~

The card is sde1.


### Copy the image to the SD card

~~~bash
cd ~/Downloads
sudo dd bs=4M if=2017-04-10-raspbian-jessie-lite.img of=/dev/sde status=progress
~~~

### Check the image

~~~bash
sudo dd bs=4M if=/dev/sde of=from-sd-card.img status=progress
truncate --reference 2017-04-10-raspbian-jessie.img from-sd-card.img
diff -s from-sd-card.img 2017-04-10-raspbian-jessie.img
~~~

Run sync. This will ensure the write cache is flushed and that it is
safe to unmount your SD card.


## Add dependencies

Start your Raspberry Pi with your newly created Raspbian image.

Install the dependencies :

~~~bash
sudo apt-get install -y gnupg2 gnupg-agent pinentry-curses scdaemon pcscd yubikey-personalization libusb-1.0-0-dev qrencode zbar-tools paperkey
~~~

Now you can disconnect your Raspberry Pi from the network and create the keys.
Ideally you should then destroy the SD card.

Alternatively, before creating the keys you can create an image of this raspberry
pi to ease the creation of later air-gapped systems.

You can also add a useful configuration file to create GPG keys:

~~~bash 
cat << EOF > ~/gpg.conf
use-agent
personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512
s2k-cipher-algo AES256
charset utf-8
fixed-list-mode
no-comments
no-emit-version
keyid-format 0xlong
list-options show-uid-validity
verify-options show-uid-validity
with-fingerprint
EOF
~~~

## Create an image of the raspberry pi (optional)

Source : [thepihut](https://thepihut.com/blogs/raspberry-pi-tutorials/17789160-backing-up-and-restoring-your-raspberry-pis-sd-card)


### Find the SD card name

Before inserting the SD card into the reader on your Linux PC, run the following command to find out which devices are currently available:

~~~bash
df -h
~~~

Which will return something like this:

~~~bash
Filesystem 1K-blocks Used Available Use% Mounted on
rootfs 29834204 15679020 12892692 55% /
/dev/root 29834204 15679020 12892692 55% /
devtmpfs 437856 0 437856 0% /dev
tmpfs 88432 284 88148 1% /run
tmpfs 5120 0 5120 0% /run/lock
tmpfs 176860 0 176860 0% /run/shm
/dev/mmcblk0p1 57288 14752 42536 26% /boot
~~~

Insert the SD card into a card reader and use the same df -h command to find out what is now available:

~~~bash
Filesystem 1K-blocks Used Available Use% Mounted on
rootfs 29834204 15679020 12892692 55% /
/dev/root 29834204 15679020 12892692 55% /
devtmpfs 437856 0 437856 0% /dev
tmpfs 88432 284 88148 1% /run
tmpfs 5120 0 5120 0% /run/lock
tmpfs 176860 0 176860 0% /run/shm
/dev/mmcblk0p1 57288 14752 42536 26% /boot
/dev/sda5 57288 9920 47368 18% /media/boot
/dev/sda6 6420000 2549088 3526652 42% /media/41cd5baa-7a62-4706-b8e8-02c43ccee8d9
~~~

The new device that wasn't there last time is your SD card.

The left column gives the device name of your SD card, and will look like `/dev/mmcblk0p1` or `/dev/sdb1`. The last part ('p1' or '1') is the partition number, but you want to use the whole SD card, so you need to remove that part from the name leaving `/dev/mmcblk0` or `/dev/sdb` as the disk you want to read from.

Open a terminal window and use the following to backup your SD card:

~~~
sudo dd if=/dev/sdb of=~/SDCardBackup.img
~~~

Use `sync` to ensure the computer has finished writing when the command prompts
reappears. You can then use the generated image to create your future air-gapped systems.
