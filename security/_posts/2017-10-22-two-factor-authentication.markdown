---
title: "Two factor authentication"
excerpt: "Couple a physical token to your password to enhance your security."
date: "2017-10-22 14:26:40 +0200"
tags: [Security key, Yubikey, Nitrokey, U2F, 2FA]
author_profile: false
sidebar:
  nav: "security-practices"
---

Before looking into two factor authentication, you might want to take a look at [password management]({% post_url /security/2017-10-22-password-management %}).


## 2FA : Two Factor authentication

Two factor authentication adds a physical token to your authentication. To login
to your website you will now have to prove that you possess :
 * **something only you know** : the password
 * **something only you have** : the physical token

The physical token can be found in the form of a USB key or an application that
you install.

{:.note .warning}
There also is a form of two factor authentication that uses SMS. You should not use it, it is highly insecure and deprecated.

### Activating 2FA

As of today, most websites have implemented 2FA. To use it, you need to install
an application such as `FreeOTP` or the `Google authenticator`.

Then you can login to your website and activate 2FA. A QR code will be shown, this QR code contains the information necessary to configure your application.
Scan it with your application and you're almost set! A list of recovery codes will be generated (if not, you should find how to generate them). You should write down these recovery codes and store them securely (in [Keepass]({% post_url /security/2017-10-22-password-management %}) for instance).

{:.note .info}
The application will generate temporary codes that you will have to enter in addition to your password.

Now next time you need to login to a website you will need to:
 1. Enter your username
 2. Enter your password
 3. Enter your temporary code generated with your app.

### 2FA and OTP

Most 2FA use a One Time Password (OTP) mechanism.

### TOTP

In the Time based One Time Password (TOTP) the server and the client have a shared secret key (shared via the QR code).
A one time password is generated using the shared secret key and the current time. The password is usually valid for 30 to 60 seconds.

### HOTP

The HMAC based One Time Password (HOTP) uses an event counter instead of the current time to compute the one time password.

## Universal 2nd Factor (U2F)

From the [Wikipedia]() article:
>Universal 2nd Factor (U2F) is an open authentication standard that strengthens and simplifies two-factor authentication using specialized USB or NFC devices based on similar security technology found in smart cards.While initially developed by Google and Yubico, with contribution from NXP Semiconductors, the standard is now hosted by the FIDO Alliance.

U2F is easier to use and more secure (you never enter manually the one time password).

### Activating U2F

 1. Connect to your website
 2. Activate U2F
 3. Plug in your [U2F security key](https://www.nitrokey.com)
 4. You security key generates a public / private key pair. The private key will never leave your USB key.
 5. The public key is sent to the website.
 6. Your security key has been registered.
 7. You should register a backup U2F security key.

{:.note .info}
Having a backup security key is very important, some sites force you to have OTP based 2FA in case you lose your security key, but not all of them do!

### How U2F works


{% mermaid %}
sequenceDiagram
    participant U2F_Key
    participant Computer
    participant Server
    Computer->>Server: I'm sammy
    Note right of Server: Encrypt challenge w/ sammy's public key
    Server->>Computer: Challenge encrypted w/ pub key
    Computer->>U2F_Key: Challenge encrypted w/ pub key
    Note left of U2F_Key: U2F key solving challenge
    U2F_Key->>Computer: Solved challenge encrypted w/ private key
    Computer->>Server: Solved challenge encrypted w/ private key
{% endmermaid %}

As we can see the private key never leaves the U2F key, which makes it very secure.
