---
title: "GitLab on Raspberry Pi 3"
excerpt: "If you feel like it, you can use your Raspberry Pi 3 to host a Gitlab server.
This post explains how you can do this."
resources:
  - name: Source
    link: https://raspbian-france.fr/raspberry-pi-serveur-github-gitlab/
  - name:
tags: [GitLab, Rasberry Pi, self-hosted]
type: Document
---

If you feel like it, you can use your Raspberry Pi 3 to host a Gitlab server.
This post explains how you can do this.

{% include warning.html content="__WARNING__: The Raspberry Pi 3 is not
powerful enough to host Gitlab. The application will work but will be
extremely slow."%}

## Download Raspbian

~~~bash
cd Downloads
unzip 2017-04-10-raspbian-jessie-lite.zip
# Output
Archive:  2017-04-10-raspbian-jessie-lite.zip
  inflating: 2017-04-10-raspbian-jessie-lite.img  
~~~

## Find the SD card

Plug the SD card and use `dmesg` to find it:
~~~bash
dmesg | tail
# Output
[  138.753330] CIFS VFS: Error connecting to socket. Aborting operation.
[  138.753386] CIFS VFS: Error connecting to socket. Aborting operation.
[  138.753690] CIFS VFS: cifs_mount failed w/return code = -113
[  138.753717] CIFS VFS: cifs_mount failed w/return code = -113
[  138.753725] CIFS VFS: cifs_mount failed w/return code = -113
[  138.753769] CIFS VFS: cifs_mount failed w/return code = -113
[ 4170.871943] sd 6:0:0:0: [sde] 125042688 512-byte logical blocks: (64.0 GB/59.6 GiB)
[ 4170.876516]  sde: sde1
~~~

The card is sde1.


## Copy the image to the SD card

~~~bash
cd ~/Downloads
sudo dd bs=4M if=2017-04-10-raspbian-jessie-lite.img of=/dev/sde status=progress conv=fsync
~~~

## Check the image

~~~bash
sudo dd bs=4M if=/dev/sde of=from-sd-card.img status=progress
~~~

~~~bash
sudo truncate --reference 2017-04-10-raspbian-jessie.img from-sd-card.img
sudo diff -s from-sd-card.img 2017-04-10-raspbian-jessie.img
~~~

Run sync. This will ensure the write cache is flushed and that it is
safe to unmount your SD card.

## Authorize SSH

For security reasons, SSH is disabled by default on raspbian now.
We can enable it by adding an empty file named `ssh` in `/mnt/rasp`.

~~~bash
sudo mkdir /mnt/rasp
sudo mount /dev/sde1 /mnt/rasp
sudo touch /mnt/rasp/ssh
umount /dev/sde1 /dev/sde1
~~~
{% include warning.html content="**Warning**: if the RPi is not restarted properly before the first successful SSH connection, the SD card will have to be re-imaged.
In short, don't try to connect the raspberry pi before putting the ssh file" %}

## Configure

Place the card in the raspberry pi and plug the power.

~~~bash
ssh pi@raspberrypi
# password is raspberrypi
~~~

Add a new user, add a ssh key and disable password authentication.

~~~bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git tree vim cifs-utils rpi-update
sudo rpi-update
~~~

## Auto mount NAS folders

Configure your NFS server as NO_ROOT_SQUASH.
~~~bash
sudo vim /etc/fstab
~~~

Add the following line, provided that you want to mount `Git/Gitlab` from your
NAS called `myNas` to `/home/sammy.GitLab`.

~~~vim
myNas:Git/Gitlab        /home/sammy/GitLab  nfs4
~~~

Remount everything to take your changes into account:
~~~bash
sudo mount -a
~~~

## Install GitLab

~~~bash
sudo apt-get install curl openssh-server ca-certificates postfix apt-transport-https

curl https://packages.gitlab.com/gpg.key | sudo apt-key add -

sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash

sudo apt-get install gitlab-ce

sudo gitlab-ctl reconfigure
~~~

### Connect to Gitlab
We assume your Raspberry Pi hostname is `homp02` on the network `home`.
Go to http://homp02.home, and use the user **root** to connect,
change the password at the first visit.

### Configure external_url

The external_url is displayed in mails sent by Gitlab.
In `/etc/gitlab/gitlab.rb` change:

{% highlight2 ruby caption=/etc/gitlab/gitlab.rb %}
external_url 'http://raspberrypi'
{% endhighlight2 %}

to

{% highlight2 ruby caption=/etc/gitlab/gitlab.rb %}
external_url 'http://homp02.home'
{% endhighlight2 %}

### Configure SMTP

You can find more information about how you can configure the SMTP service
in [the official documentation](https://docs.gitlab.com/omnibus/settings/smtp.html).
Here is the configuration I used for OVH:

{% highlight2 ruby caption=/etc/gitlab/gitlab.rb %}
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "ssl0.ovh.net"
gitlab_rails['smtp_port'] = 465
gitlab_rails['smtp_user_name'] = "mail@sammy.fr"
gitlab_rails['smtp_password'] = "password"
gitlab_rails['smtp_domain'] = "ssl0.ovh.net"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = true
gitlab_rails['smtp_openssl_verify_mode'] = 'none'

gitlab_rails['gitlab_email_from'] = 'mail@sammy.fr'
gitlab_rails['gitlab_email_reply_to'] = 'noreply@sammy.fr'
{% endhighlight2 %}

### Configure alternative directory

https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/configuration.md#storing-git-data-in-an-alternative-directory

Replace in `/etc/gitlab/gitlab.rb`

{% highlight2 ruby caption=/etc/gitlab/gitlab.rb %}
git_data_dirs({ "default" =>  { "path" => "/var/opt/gitlab/git-data", 'gitaly_address' => 'unix:/var/opt/gitlab/gitaly/gitaly.socket' }
})
{% endhighlight2 %}

by


{% highlight2 ruby caption=/etc/gitlab/gitlab.rb %}
git_data_dirs({ "default" => { "path" => "/home/sammy/GitLab" },
        "alternative" => { "path" => "/var/opt/gitlab/git-data", 'gitaly_address' => 'unix:/var/opt/gitlab/gitaly/gitaly.socket' }
})
{% endhighlight2 %}

Run `sudo gitlab-ctl reconfigure`

## Configure Unicorn workers

If you need to adjust the Unicorn timeout or the number of workers you can use
the following settings in /etc/gitlab/gitlab.rb.
Run sudo gitlab-ctl reconfigure for the change to take effect.


{% highlight2 ruby caption=/etc/gitlab/gitlab.rb %}
unicorn['worker_processes'] = 2
unicorn['worker_timeout'] = 60
{% endhighlight2 %}
